# iac-demo

[![pipeline status](https://gitlab.com/Nklya/iac-demo/badges/master/pipeline.svg)](https://gitlab.com/Nklya/iac-demo/commits/master)

IaC demo

* First
  * `ansible all -m ping -i inventory.yml -u vagrant --private-key=~/.vagrant.d/insecure_private_key`
  * `ansible-playbook playbook.yml -i inventory.yml -u vagrant --private-key=~/.vagrant.d/insecure_private_key`
  * `ansible-playbook playbook.yml`
